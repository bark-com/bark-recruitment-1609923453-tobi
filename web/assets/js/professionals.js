const apiHost = 'http://localhost:4000'; // used for docker
// const apiHost = 'http://bark-recruit-api.local'; // used for vagrant

// onload
$(document).ready(function(){

    $('.js-autocomplete-services').autoComplete({
        resolverSettings: {
            url: `${apiHost}/api/services`
        }
    });

    $('.js-autocomplete-location').autoComplete({
        resolverSettings: {
            url: `${apiHost}/api/locations`
        }
    });

    (new LeadsList).init()

});

class LeadsList {

    init() {
        this.loadList();
        this.initDomItems();
        this.templateListItem = Handlebars.compile($('#handlebars-template-list-item').html());
        this.templateListDetail = Handlebars.compile($('#handlebars-template-list-detail').html());
        this.initListeners();
    }

    initDomItems() {
        this.leadListDom = $('#js-lead-list');
        this.leadDetailDom = $('#js-lead-details');
        this.leadDetailContainerDom = $('#js-lead-details-container');
        this.filterServiceDom = $('input[name=service_id]');
        this.filterLocationDom = $('input[name=location_id]');
        this.searchButtonDom = $('#js-search-button');
    }

    initListeners() {
        this.leadListDom.on(
            'click', '.js-list-item',
            (event) => this.loadLeadDetail($(event.currentTarget).data('leadId'))
        );
        this.searchButtonDom.click((event) => {
            event.preventDefault();
            this.loadList()
        });
    }

    loadLeadDetail(id) {
        this.getLeadFromApi(id)
            .then(response => response.json())
            .then(lead => this.renderLead(lead));
    }

    loadList() {
        this.getListFromApi()
            .then(response => response.json())
            .then(leadList => this.renderList(leadList));
    }

    renderLead(lead) {
        this.leadDetailContainerDom.removeClass('d-none');
        this.leadDetailDom.html(this.templateListDetail(lead));
    }

    renderList(leadList) {
        this.leadListDom.html('');
        for (let lead of leadList) {
            if (this.displayLead(lead)) {
                this.leadListDom.append(this.templateListItem(lead))
            }
        }
    }

    displayLead(lead) {
        let serviceIdFilter = parseInt(this.filterServiceDom.val());
        let locationIdFilter = parseInt(this.filterLocationDom.val());

        if (serviceIdFilter && lead.service_id !== serviceIdFilter) {
            return false;
        }
        if (locationIdFilter && lead.location_id !== locationIdFilter) {
            return false;
        }

        return true;
    }

    getListFromApi() {
        return fetch(
            `${apiHost}/api/leads`,
            {
                method: 'GET'
            }
        )
    }

    getLeadFromApi(id) {
        return fetch(
            `${apiHost}/api/leads/${id}`,
            {
                method: 'GET'
            }
        )
    }

}
